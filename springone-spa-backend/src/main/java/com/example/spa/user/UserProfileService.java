package com.example.spa.user;

import static com.google.common.base.Preconditions.checkNotNull;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

@Service
public class UserProfileService implements ApplicationListener<ApplicationEvent>
{
	// @Autowired
	UserDetailsService userDetailsService;
	
	// autowire services or repositories needed to be able to get a profile 

	private final LoadingCache<UserId, UserProfile> userProfileCache = CacheBuilder.newBuilder().maximumSize(1000).build(
			new CacheLoader<UserId, UserProfile>() {

				@Override
				public UserProfile load(final UserId key) throws Exception
				{
					checkNotNull(key);
					UserDetails userDetails = null; // get this from the UserDetailsService 
					UserAccount userAccounts = null; // get this value from the database or some other service 
					return new UserProfile(userDetails,userAccounts);

				}
			});

	public UserProfile currentUserProfile()
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object user = authentication.getPrincipal();
		return this.userProfileCache.getUnchecked((UserId) user);
	}

	/**
	 * Listen for events that require invalidating the cache
	 */
	@Override
	public void onApplicationEvent(ApplicationEvent event)
	{
		UserId userId = null; // get this from the event object
		//this.userProfileCache.invalidate(userId);
	}

}
